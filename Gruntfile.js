module.exports = function(grunt) {

	grunt.initConfig({
		pkg : grunt.file.readJSON('package.json'),
		sass: {
		     dist: {
				 files: {
					'styles/main.css': 'styles/main.sass'
				}
			}
		},
		ftp_push: {
		    demo: {
		    	options: {
		    		authKey: 'netology',
		    		host: 'university.netology.ru',
		    		dest: '/fbb-store/',
		    		port: 21
		    	},
		    	files: [{
		    		expand: true,
		    		cwd: '.',
		    		src: [
		    		      'main-page.html',
		    		      'styles/main-page.css'
		    		]
		        }]
		    }
		 }
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-ftp-push');
	
	grunt.registerTask('default', ['sass', 'ftp_push']);
	grunt.registerTask('start', ['sass']);

};