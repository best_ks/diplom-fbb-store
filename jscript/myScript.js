// находим коллекцию элементов с классом 'book'
var booksParent = document.getElementById('books');
var book = document.getElementsByClassName('book');

//находим кнопку "Еще несколько книг"
var btnOneMoreBook = document.getElementById('btnOneMoreBook');

// переменная для получения результатов запросов
var str;
//индекс коллекции str последней отображенной на странице книги
var lastBookIndex = 0;

//назначаем обработчик загрузки страницы
window.onload = ready;

//функция-обратчик загрузки страницы
function ready (event) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", 'https://netology-fbb-store-api.herokuapp.com/book/', true);
    xhr.send();

    xhr.onreadystatechange = function() {
        if(xhr.readyState != 4) return;

        if(xhr.status != 200){
            // ошибка
            str = xhr.responseText;
            str = JSON.parse(str);
            alert('Error ' + str.error.code + ": " + str.error.message);
        }
        else{
            // результат
            str = xhr.responseText;
            str = JSON.parse(str);
			
			[].forEach.call(book,function(elem, i){
				//получение id книги
				elem.setAttribute('id',str[i].id);
                //установка обложки книги
				elem.children[0].firstElementChild.setAttribute('src', str[i].cover.small);
                //вставляем краткую информацию о книге
                elem.children[1].innerHTML = str[i].info;
				//назначение обработчика события 'click' по обложке книги
				elem.children[0].firstElementChild.addEventListener('click',bookClickListener);
				//проверяем, является ли <section> elem последним элементом для <section class="four-books clearfix">
				if(elem === document.getElementsByClassName('four-books')[0].lastElementChild){
					lastBookIndex = i;
				}
			});
        }
    }
}

//обрабатываем нажатие кнопки "Еще несколько книг"
btnOneMoreBook.onclick = function (event) {
	//находим <section class="four-books clearfix">, в которую будем добавлять книги
	var booksSection = document.getElementsByClassName('four-books clearfix')[0];
	
	//в цикле добавляем книгу и проверяем, есть ли еще книги в коллекции str
	do{
		lastBookIndex++;
		//клонируем <section class="book">
		var cloneBook = document.getElementsByClassName('book')[0].cloneNode('true');
		//добавляем клон в <section class="four-books clearfix">
		booksSection.appendChild(cloneBook);
		//заполняем клон данными:
		//получение id книги
		cloneBook.setAttribute('id',str[lastBookIndex].id);
		//вставляем обложку книги
		cloneBook.children[0].firstElementChild.setAttribute('src', str[lastBookIndex].cover.small);
		//вставляем краткую информацию о книге
		cloneBook.children[1].innerHTML = str[lastBookIndex].info;
		//назначение обработчика события 'click' по обложке книги
		cloneBook.children[0].firstElementChild.addEventListener('click',bookClickListener);
	}
	while((str.length - (lastBookIndex + 1)) != 0 && (lastBookIndex + 1) % 4 != 0);
	
	//если книг в коллекции str больше нет, то скрываем кнопку
	if((str.length - (lastBookIndex + 1)) == 0){
		btnOneMoreBook.hidden = 'true';
	}
}

function bookClickListener(event){
	//получение id выбранной книги
	var booksID = this.parentNode.parentNode.getAttribute('id');
	//переход на страницу выбранной книги
	location.assign('book-view-page.html?'+booksID);
}