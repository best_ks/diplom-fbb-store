// находим <section> с классом 'first-book'
var book = document.getElementById('first-book');
//находим элемент <div class="buyBtn">
var btn = document.getElementsByClassName('buyBtn')[0];
// переменная для получения результатов запросов
var str;
// переменная для хранения id выбранной книги
var booksID;

// находим блоки <section id="review"> и первый <section class="advantages" >
// в блоке <div class="rightSide"> для коррекции высоты этих блоков.
// Расчет производится в функции setSizes()
var reviewBlock = document.getElementById('review');
var advantageBlock = document.getElementsByClassName('rightSide')[0].firstElementChild;

//назначаем обработчик загрузки страницы
window.onload = ready;
//назначаем обработчик нажания кнопки <div class="buyBtn">
btn.addEventListener('click',btnClickListener);

//функция-обратчик загрузки страницы
function ready (event) {
	
    var xhr = new XMLHttpRequest();
    xhr.open("GET", 'https://netology-fbb-store-api.herokuapp.com/book/' + location.search.substring(1), true);
    xhr.send();

    xhr.onreadystatechange = function() {
        if(xhr.readyState != 4) return;

        if(xhr.status != 200){
            // ошибка
            str = xhr.responseText;
            str = JSON.parse(str);
            alert('Error ' + str.error.code + ": " + str.error.message);
        }
        else{
            // результат
            str = xhr.responseText;
            str = JSON.parse(str);

            //запоминаем id выбранной книги
			booksID = location.search.substring(1);

			// установка обложки книги
            book.children[0].firstElementChild.setAttribute('src', str.cover.large);

            //вставляем краткую информацию о книге
            book.children[1].innerHTML = str.description;

			//смотрим отзывы
            var reviewSections = document.getElementsByClassName('leftSide')[0];
            [].forEach.call(str.reviews,function (review, i) {
				reviewSections.children[i].getElementsByTagName('img')[0].setAttribute('src',review.author.pic);
                reviewSections.children[i].getElementsByTagName('p')[0].innerHTML = review.cite;
			});

            //смотрим отзывы
            var featuresSections = document.getElementsByClassName('rightSide')[0];
            [].forEach.call(str.features,function (feature, i) {
				featuresSections.children[i].getElementsByTagName('img')[0].setAttribute('src',feature.pic);
				featuresSections.children[i].getElementsByTagName('p')[0].innerHTML = feature.title;
			});
		}

		//устанавливаем размеры элементов на странице
		if(window.screen.width >= 980)
			setSizes();
    }
}

function setSizes(){
	//1. Находим отступы для блока <section id="first-book">
	//находим отступ <body> от левого края документа
	var computedStyle = getComputedStyle(document.body);
	var bodyLeftMargin = parseInt(computedStyle.marginLeft);
	//к нему же добавим paddingLeft <body>
	bodyLeftMargin += parseInt(computedStyle.paddingLeft);
	
	//берем ширину блока <div class="leftSide">
	var leftSideBlock = document.getElementsByClassName('leftSide')[0];
	computedStyle = getComputedStyle(leftSideBlock);
	var leftSideBlockWidth = parseInt(computedStyle.width);
	var leftSideBlockHeight = computedStyle.height; //для минимальной высоты родительского блока
	
	//и применяем эти значения к стилям marginLeft и marginRight для блока <section id="first-book">
	book.style.left = bodyLeftMargin + leftSideBlockWidth + "px";
	book.style.right = book.style.left;
	
	//2. Устанавливаем ширину кнопки <div class="buyBtn"> и надписи в <section id="skraffi">, 
	// равными ширине обложки книги (<div> в <section id="first-book">)
	computedStyle = getComputedStyle(book.firstElementChild);
	var bookWidth = computedStyle.width;	
	var btn = document.getElementsByClassName('buyBtn')[0];
	var skraffi = document.getElementById('skraffi');
	btn.style.width = bookWidth;
	skraffi.style.width = bookWidth;
	
	//3. Устанавливаем высоту блока <div class="block clearfix"> по высоте <section id="first-book">
	computedStyle = getComputedStyle(book);
	var bookHeight = computedStyle.height;
	book.parentElement.style.height = bookHeight;
	book.parentElement.style.minHeight = leftSideBlockHeight;
	
	//4. Выравниваем высоту блока <section id="review"> и первого блока <section class="advantages" >
	// в блоке <div class="rightSide">
	computedStyle = getComputedStyle(reviewBlock);
	var reviewBlockHeight = parseInt(computedStyle.height);
	computedStyle = getComputedStyle(advantageBlock);
	var advantageBlockHeight = parseInt(computedStyle.height);
	
	// находим разницу этих высот
	var heightDifference = reviewBlockHeight - advantageBlockHeight;
	
	// корректируем высоту того или другого блока
	if(heightDifference > 0){
		advantageBlockHeight += heightDifference;
		advantageBlock.style.height = advantageBlockHeight + "px";
	}
	else if(heightDifference < 0){
		reviewBlockHeight -= heightDifference;
		reviewBlock.style.height = reviewBlockHeight + "px";
	}
}

window.onresize = function(event){
	if(window.screen.width >= 980)
		setSizes();
}

function btnClickListener(event){
	//переход на страницу выбранной книги
	location.assign('buy-book-form-page.html?' + booksID);
}