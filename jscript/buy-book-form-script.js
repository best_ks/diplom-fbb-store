// находим <img> в блоке <div class = 'pic'>
var book = document.getElementsByClassName('pic')[0].firstElementChild;
// находим <u> в блоке <p> с названием книги
var bookName = document.getElementById('bookName');
// находим блок <p>, куда запишем  итоговую цену
var bookPriceBlock = document.getElementById('bookPrice');
// определяем <form>, чтобы работать с её элементами
var form = document.forms[0];

// id выбранной книги
var booksID;
// стоимость книги
var bookPrice = 0;
// id валюты книги
var bookCurrencyID;
// id выбранной доставки
var deliveryID;
// id валюты доставки
var deliveryCurrencyID;
// адрес доставки
var deliveryAddress = '';
// стоимость доставки
var deliveryPrice = 0;
// список доставок
var deliveryPriceList;
// итоговая цена за книгу
var endPrice;
// курсы валют
var currencyThisDay;
// id валюты, в которой оформляется заказ
var сurrencyID;
// id способа оплаты
var paymentID;

// назначаем обработчик отправки формы
form.addEventListener('submit',onSubmitForm);

// назначаем обработчик нажания кнопки <div class="buyBtn">
bookName.addEventListener('click',onBookClickListener);

// назначаем обработчик загрузки страницы
window.onload = ready;

// функция-обратчик загрузки страницы
function ready (event) {
    // запрашиваем курсы валют на текущую дату
    requestFunction('https://netology-fbb-store-api.herokuapp.com/currency/', 'currency');
}

// Функция обработки запроса
function requestFunction(requestString, key) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", requestString, true);
    xhr.send();

    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;

        if (xhr.status != 200) {
            // ошибка
            str = xhr.responseText;
            str = JSON.parse(str);
            alert('Error ' + str.error.code + ": " + str.error.message);
        }
        else {
            // обработка запроса
            str = xhr.responseText;
            str = JSON.parse(str);

            // запрос для валюты
            if(key == 'currency'){
                currencyThisDay = str;

                // запрашиваем данные о выбранной книге
                requestFunction('https://netology-fbb-store-api.herokuapp.com/book/' + location.search.substring(1), 'book');

                // запрашиваем возможные способы доставки и создаем элементы <input> по выбору доставки
                requestFunction('https://netology-fbb-store-api.herokuapp.com/order/delivery', 'delivery');

                // запрашиваем возможные способы оплаты и создаем элементы <input> по выбору оплаты
                requestFunction('https://netology-fbb-store-api.herokuapp.com/order/payment', 'payment');
            }

            // запрос по выбранной книге
            if(key == 'book') {
                // запоминаем id выбранной книги
                booksID = str.id;
                // установка обложки книги
                book.setAttribute('src', str.cover.small);
                // вставляем название книги
                bookName.innerHTML = str.title;
                // запоминаем цену на книгу
                bookPrice = str.price;
                // id валюты за книгу
                bookCurrencyID = str.currency;
                // запоминаем валюту
                //bookPriceCharCode = currencyFunction(bookCurrencyID);

                setPrices();
                setSizes();
            }

            // запрос по доставке
            if(key == 'delivery'){
                deliveryPriceList = str;
                // создаем <input> в блоке <div class = "firstElem"> блока <div class = "secondRow">
                [].forEach.call(deliveryPriceList,function(elem) {
                    var charCode = currencyFunction(elem.currency);
                    createInputs(document.getElementsByClassName('firstElem')[1], 'delivery', elem.id, elem.name, elem.price, charCode);
                });

                var deliveryInputs = form.elements.delivery;
                [].forEach.call(deliveryInputs, function (elem) {
                    elem.addEventListener('click', onDeliveryInputClickListener);
                });
            }

            // запрос по способу оплаты для доставки
            if(key == 'paymentForDelivery'){
                var paymentInputs = form.elements.payment;

                [].forEach.call(paymentInputs, function (paymentInput) {
                    paymentInput.disabled = true;
                    paymentInput.checked = false;
                    paymentInput.nextElementSibling.style.color = 'grey';
                });
                [].forEach.call(paymentInputs, function (paymentInput) {
                    [].forEach.call(str, function (strObject) {
                        if(paymentInput.id == strObject.id) {
                            paymentInput.disabled = false;
                            paymentInput.nextElementSibling.style.color = 'black';
                        }
                    });
                });
            }

            // запрос по способу оплаты
            if(key == 'payment'){
                // создаем <input> в блоке <div class = "secondElem"> блока <div class = "secondRow">
                [].forEach.call(str,function(elem) {
                    createInputs(document.getElementsByClassName('secondElem')[1], 'payment', elem.id, elem.title);
                });

                var paymentInputs = form.elements.payment;
                [].forEach.call(paymentInputs, function (elem) {
                    elem.addEventListener('click', onPaymentInputClickListener);
                });
            }
        }
    }
}

// функция обрботки валюты
function currencyFunction(id){
    var charCode;
    [].forEach.call(currencyThisDay, function (elem) {
        if(elem.ID == id)
            charCode = elem.CharCode;
    });
    return charCode;
}

// функция создания <input> в соответствующем блоке block
function createInputs(block, radioName, idInput, name, price, code){
    var label = document.createElement('label');
    var br = document.createElement('br');
    var input = document.createElement('input');
    label.setAttribute('for',idInput);
    input.setAttribute('type', 'radio');
    input.setAttribute('id',idInput);
    input.setAttribute('name', radioName);

    block.appendChild(input);
    block.appendChild(label);
    if(price == undefined)
        label.innerText = name;
    else
        label.innerText = name + ' - ' + price + ' ' + code;
    block.appendChild(br);
}

// функция-обработчик способа доставки
function onDeliveryInputClickListener(event){
    requestFunction('https://netology-fbb-store-api.herokuapp.com/order/delivery/' + this.id + '/payment', 'paymentForDelivery');

    deliveryID = this.id;
    var deliveryProcess = deliveryPriceList.find(function(item) {
        return item.id === deliveryID;
    });

    deliveryPrice = deliveryProcess.price;
    deliveryCurrencyID = deliveryProcess.currency;
    if (deliveryProcess.needAdress) {
        if(!form.elements.address) {
            var label = document.createElement('label');
            var addressArea = document.createElement('textarea');
            addressArea.setAttribute('name', 'address');
            document.getElementsByClassName('firstElem')[1].appendChild(label);
            label.innerHTML = 'Адрес доставки:<br>';
            label.appendChild(addressArea);
        }
    }
    else{
        if(form.elements.address) {
            form.elements.address.parentElement.remove();
            deliveryAddress = '';
        }
    }

    setPrices();
}

// функция-обработчик способа оплаты
function onPaymentInputClickListener(event){
    paymentID = this.id;
}

function onBookClickListener(event){
    //переход на страницу выбранной книги
    location.assign('book-view-page.html?'+booksID);
}

function setPrices() {
    // итоговая цена
    endPrice = bookPrice + deliveryPrice;
    if(bookCurrencyID == deliveryCurrencyID || deliveryPrice == 0 ) {
        var charCode = currencyFunction(bookCurrencyID);
        сurrencyID = bookCurrencyID;
    }
    // выводим итоговую цену в выбранной валюте
    bookPriceBlock.innerHTML = "Итого к оплате: " + endPrice + " " + charCode;
}

function setSizes(){

    //1. Находим высоту блока <div class = "firstElem"> в блоке <div class = "firstRow">
    var computedStyle = getComputedStyle(document.getElementsByClassName('firstRow')[0].firstElementChild);
    var firstElemHeight = computedStyle.height;

    // находим высоту всего <label> в блоке <div class = "secondElem"> блока <div class = "firstRow">
    computedStyle = getComputedStyle(document.getElementsByClassName('firstRow')[0].lastElementChild.firstElementChild);
    var labelHeight = computedStyle.height;

    // находим текущую высоту всего <textarea>
    computedStyle = getComputedStyle(form.elements.comment);
    var textareaHeight = computedStyle.height;

    // находим высоту заголовка для <textarea>
    labelHeight -= textareaHeight;

    // и устанавливаем новую высоту для <textarea> блока <div class = "secondElem"> в блоке <div class = "firstRow">
    form.elements.comment.style.height = firstElemHeight - labelHeight;

    //2. Находим ширину блока <p  id = "bookPrice">
    computedStyle = getComputedStyle(document.getElementById('bookPrice'));
    var bookPriceWidth = computedStyle.width;

    // и устанавливаем такую же ширину для кнопки <input name = "submitBtn"  type="submit" value="Оформить заказ">
    form.elements.submitBtn.style.width = bookPriceWidth;
}

// функция отправки формы
function onSubmitForm(event) {
    event.preventDefault();

    var check = validate(form);

    if(check){
        var xhr = new XMLHttpRequest();

        var json = JSON.stringify({
            manager: 'best.bros.kest@gmail.com', // ваш адрес электронной почты для получения письма с заказом
            book: booksID, // идентификатор книги
            name: form.elements.name.value, // имя
            phone: form.elements.phone.value, // контактный телефон
            email: form.elements.email.value, // адрес электронной почты клиента, на него письма не отсылаются
            comment: form.elements.comment.value, // комментарий к заказу
            delivery: { // информация о доставке
                id: deliveryID, // идентификатор способа доставки
                address: deliveryAddress // адрес доставки, если требуется
            },
            payment: { // информация об оплате
                id: paymentID, // идентификатор выбранного способа оплаты
                currency: сurrencyID // идентификатор валюты, в который оформляется заказ
            }
        });

        xhr.open("POST", 'https://netology-fbb-store-api.herokuapp.com/order', true)
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.send(json);

        xhr.onreadystatechange = function() {
            if (xhr.readyState != 4) return;

            if (xhr.status != 200) {
                // ошибка
                str = xhr.responseText;
                str = JSON.parse(str);
                //alert('Status: ' + str.status + "\nMessage: " + str.message);
                form.remove();
                var footer = document.getElementsByTagName('footer')[0];
                var newElement = document.createElement('p');
                newElement.innerHTML = 'Ваш заказ не получается обработать...<br>Спасибо за попытку.';
                document.body.insertBefore(newElement, footer);
            }
            else {
                // обработка запроса
                str = xhr.responseText;
                str = JSON.parse(str);
                form.remove();
                var footer = document.getElementsByTagName('footer')[0];
                var newElement = document.createElement('p');
                newElement.innerHTML = 'Заказ на книгу успешно оформлен.<br>Спасибо, что спасли книгу от сжигания в печи.';
                document.body.insertBefore(newElement, footer);
            }
        };
    }
}

// функция проверки введенных в форму данных
function validate(thisForm) {
    var elems = thisForm.elements;

    resetError(elems.name.parentNode);
    var nameCheck = true;
    if (!elems.name.value) {
        showError(elems.name.parentNode, ' Введите Ваше имя.');
        nameCheck = false;
    }

    resetError(elems.phone.parentNode);
    var phoneCheck = true;
    if (!elems.phone.value) {
        showError(elems.phone.parentNode, ' Укажите свой номер телефона<br>(Только цифры!).');
        phoneCheck = false;
    }
    else if (isNaN(elems.phone.value)) {
        showError(elems.phone.parentNode, ' Только цифры!');
        phoneCheck = false;
    }

    resetError(elems.email.parentNode);
    var emaileCheck = true;
    if (!elems.email.value) {
        showError(elems.email.parentNode, ' Укажите адрес эл. почты.');
        emaileCheck = false;
    }

    resetError(elems.comment.parentNode);
    var commentCheck = true;
    if (!elems.comment.value) {
        showError(elems.comment.parentNode, ' Хотите - напишите комментарий,<br>хотите -нет.<br>Но лучше написать!');
        commentCheck = false;
    }

    resetError(elems.delivery[0].parentNode);
    var deliveryCheck = false;
    [].forEach.call(elems.delivery, function (elem) {
        if(elem.checked)
            deliveryCheck = true;
    });
    if (!deliveryCheck) {
        showError(elems.delivery[0].parentNode, ' Вы не выбрали способ доставки!');
    }

    if(elems.address) {
        resetError(elems.address.parentNode);
        var addressCheck = true;
        if (!elems.address.value) {
            showError(elems.address.parentNode, ' Добавьте адрес доставки!');
            addressCheck = false;
        }
    }

    resetError(elems.payment[0].parentNode);
    var paymentCheck = false;
    [].forEach.call(elems.payment, function (elem) {
        if(elem.checked)
            paymentCheck = true;
    });
    if (!paymentCheck) {
        showError(elems.payment[0].parentNode, ' Вы не выбрали способ доставки!');
    }

    if(elems.address) {
        if (nameCheck && phoneCheck && emaileCheck && commentCheck && addressCheck && deliveryCheck && paymentCheck) {
            deliveryAddress = elems.address.value;
            return true;
        }
        else
            return false;
    }
    else{
        if (nameCheck && phoneCheck && emaileCheck && commentCheck && deliveryCheck && paymentCheck)
            return true;
        else
            return false;
    }
}

// функция вывода ошибки
function showError(container, errorMessage) {
    container.classList.add('error');
    var msgElem = document.createElement('div');
    msgElem.className = "error-message";
    msgElem.innerHTML = errorMessage;
    if(container == form.elements.delivery[0].parentNode || container == form.elements.payment[0].parentNode)
        container.firstElementChild.appendChild(msgElem);
    else
        container.appendChild(msgElem);
}

// функция сброса ошибок
function resetError(container) {
    container.classList.remove('error');
    if(container == form.elements.delivery[0].parentNode || container == form.elements.payment[0].parentNode) {
        if (container.firstElementChild.lastChild.className == "error-message") {
            container.firstElementChild.removeChild(container.firstElementChild.lastChild);
        }
    }
    else if (container.lastChild.className == "error-message") {
        container.removeChild(container.lastChild);
    }
}